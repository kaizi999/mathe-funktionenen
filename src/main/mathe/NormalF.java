/*
Copyright Kai-Uwe Zimdars 2015
 */
package mathe;

/**
 *
 * @author Kai-Uwe
 */
public class NormalF {

    double a;
    double p;
    double q;
    
    public NormalF(double a, double p, double q) {
        this.a = a;
        this.p = p;
        this.q = q;
    }
    
    public double getA() {
        return a;
    }
    
    public double getP() {
        return p;
    }
    
    public double getQ() {
        return q;
    }
    
    public static NormalF getNormalF(SPF spf) {
        double a = spf.getA();
        double p = spf.getVerschiebungX() * 2;
        double q = Math.pow(spf.getVerschiebungX(), 2) + spf.getVerschiebungY();
        return new NormalF(a, p, q);
    }
}
