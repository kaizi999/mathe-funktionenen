/*
Copyright Kai-Uwe Zimdars 2015
 */
package mathe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import mathe.GUI.MainFrame;

/**
 *
 * @author Kai-Uwe
 */
public class Mathe {

    public Funktionen aktuelleFunktion;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Programm wird gestartet...");
        System.out.println();
        System.out.println("Willkommen bei dem Programm zur Berechnung von Funktionen!  (Copyright Kai-Uwe Zimdars 2015)");
        System.out.println("Wie möchten Sie die Funktion definieren?");
        System.out.println("1: Mit der Scheitelpunktsform");
        System.out.println("2: Mit p und q");
        System.out.print("Auswahl: ");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        int auswahl = 0;
        try {
            auswahl = Integer.parseInt(br.readLine());
        } catch(IOException e) {
            System.exit(1);
        }
        
        System.out.println();
                
        switch(auswahl) {
            case 1: new Mathe().mitSPFDefinieren(br); break;
            case 2: new Mathe().mitPQDefinieren(br); break;
        }
    }
    
    public void mitSPFDefinieren(BufferedReader br) {
        System.out.println("Geben Sie jetzt nacheinander folgende Werte ein: a, verschiebungX, verschiebungY");
        double a = 0;
        double verschiebungX = 0;
        double verschiebungY = 0;
        
        try {
            System.out.print("a: ");
            a = Double.parseDouble(br.readLine());
            System.out.println();
            System.out.print("verschiebungX: ");
            verschiebungX = Double.parseDouble(br.readLine());
            System.out.println();
            System.out.print("verschiebungY: ");
            verschiebungY = Double.parseDouble(br.readLine());
            System.out.println();
            aktuelleFunktion =  new Funktionen(new SPF(a, verschiebungX, verschiebungY));
        } catch(IOException e) {
            System.exit(1);
        }
        
        programmStarten(br, aktuelleFunktion);
    }
    
    public void mitPQDefinieren(BufferedReader br) {
        System.out.println("Geben Sie jetzt nacheinander folgende Werte ein: a, p, q");
        double a = 0;
        double p = 0;
        double q = 0;
        
        try {
            System.out.print("a: ");
            a = Double.parseDouble(br.readLine());
            System.out.println();
            System.out.print("p: ");
            p = Double.parseDouble(br.readLine());
            System.out.println();
            System.out.print("q: ");
            q = Double.parseDouble(br.readLine());
            System.out.println();
            aktuelleFunktion =  new Funktionen(new NormalF(a, p, q));
        } catch(IOException e) {
            System.exit(1);
        }

        programmStarten(br, aktuelleFunktion);
    }

    public void programmStarten(BufferedReader br, Funktionen f) {
        System.out.println("Möchten sie ein Grafisches User Interface (GUI) oder eine Konsolenanwendung?");
        System.out.println("Die Konsolenanwendung hat jedoch viel mehr Funktionen als die GUI");
        System.out.println("1: GUI");
        System.out.println("2: Konsolenanwendung");
        System.out.print("Auswahl: ");

        int auswahl = 0;
        try {
            auswahl = Integer.parseInt(br.readLine());
        } catch(IOException e) {
            System.exit(1);
        }
        
        System.out.println();
                
        switch(auswahl) {
            case 1: GUIAufrufen(f); break;
            case 2: konsoleAufrufen(f, br); break;
        }

    }

    public void konsoleAufrufen(Funktionen f, BufferedReader br) {
        new Menue(f, br);
    } 

    public void GUIAufrufen(Funktionen f) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame(f).setVisible(true);
            }
        });
    } 
}
