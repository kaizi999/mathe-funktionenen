/*
Copyright Kai-Uwe Zimdars 2015
 */
package mathe;

/**
 *
 * @author Kai-Uwe
 */
public class Point2D {
    private double x;
    private double y;
    
    public Point2D(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public double getX() {
        return x;
    }
    
    public double getY() {
        return y;
    }
    
    @Override
    public String toString() {
        return "x = " + x + ", y = " + y;
    }
}
