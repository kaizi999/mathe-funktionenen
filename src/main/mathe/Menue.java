/*
Copyright Kai-Uwe Zimdars 2015
 */
package mathe;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;

/**
 *
 * @author Kai-Uwe
 */
public class Menue {

    Funktionen aktuelleFunktion;
    BufferedReader br;
    
    public Menue(Funktionen f, BufferedReader br) {
        aktuelleFunktion = f;
        this.br = br;
        menu();
    }
    
    public void menu() {
        scheitelpunktsformAnzeigen();
        while(true) {
            System.out.println("Was möchten Sie machen?");
            System.out.println("1: Nullstellen berechnen");
            System.out.println("2: Scheitelpunktform berechnen");
            System.out.println("3: Y-Wert berechnen");
            System.out.println("5: Program beenden");
            System.out.print("Auswahl: ");
            int auswahl = 0;
            try {
                auswahl = Integer.parseInt(br.readLine());
            } catch(IOException e) {
                System.exit(1);
            }

            System.out.println();

            switch(auswahl) {
                case 1: nullstellenBerechnen(); System.out.println(); break;
                case 2: scheitelpunktBerechnen(); System.out.println(); break;
                case 3: yWertBerechnen(); System.out.println(); break;
                case 4: System.exit(0); break;
            }
        }
    }
    
    public void yWertBerechnen() {
        System.out.println("Geben sie den X-Wert ein, zu dem der Y-Wert berechnet werden soll!");
        System.out.print("x = ");
        try {
            double x = Double.parseDouble(br.readLine());
            double y  = aktuelleFunktion.berechneY(x);
            System.out.println("Der Y-Wert ist " + y);
        } catch (IOException e) {
            System.exit(0);
        }
        
    }
    
    public void xWertBerechnen() {
        System.out.println("Diese Funkiton geht in der akteullen Version leider nicht :(");
    }

    public void nullstellenBerechnen() {
        System.out.println("Nullstellen werden berechnet...");
        Point2D[] nullstellen = aktuelleFunktion.abcNullstellen();
        if(nullstellen == null) {
            System.out.println("Diese Funktion hat keine Nullstellen!");
        } else if(nullstellen.length == 1) {
            System.out.println("Diese Funktion hat eine Nullstelle: " + nullstellen[0]);
        } else {
            System.out.println("Diese Funktion hat zwei Nullstellen: ");
            System.out.println("Nullstelle 1: " + nullstellen[0]);
            System.out.println("Nullstelle 2: " + nullstellen[1]);
        }
    }
    
    public void scheitelpunktBerechnen() {
        System.out.println("Scheitelpunkt wird berechnet...");
        System.out.println("Scheitelpunkt: " + aktuelleFunktion.getScheitel());
    }

    public void scheitelpunktsformAnzeigen() {
        System.out.println(aktuelleFunktion.getSPF().toString());
    }
}