/*
Copyright Kai-Uwe Zimdars 2015
 */

package mathe;

import com.sun.corba.se.impl.orb.NormalDataCollector;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author Kai-Uwe
 */
public class Funktionen {
    
    SPF spf;
    NormalF nf;
    
    public Funktionen(SPF spf) {
        this.spf = spf;
        this.nf = NormalF.getNormalF(spf);
    }
    
    public Funktionen(NormalF nf) {
        this.spf = SPF.getSPF(nf);
        this.nf = nf;
    }
    
    public Point2D getScheitel() {
        return new Point2D(spf.getVerschiebungX() * (-1), spf.getVerschiebungY());    
    }
    
    public double berechneY(double x) {
        return nf.getA() * Math.pow(x, 2) + nf.getP() * x + nf.getQ();  
    }
    
    public Point2D[] nullstellen() {
        double d = Math.pow(nf.getP() / 2, 2) - nf.getQ();
        if(d > 0) {
            double w = Math.sqrt(d);
            double vw = - (nf.getP() / 2);
            
            double x1 = vw + w;
            double x2 = vw - w;
            
            Point2D[] returnArray = new Point2D[2];
            returnArray[0] = new Point2D(x1, 0);
            returnArray[1] = new Point2D(x2, 0);
            
            return returnArray;
        } else if(d == 0) {
            Point2D[] returnArray = new Point2D[1];
            double vd = - (nf.getP() / 2);
            returnArray[0] = new Point2D(vd, 0);
            return returnArray;
        } else {
            return null;
        }
    }

    public Point2D[] abcNullstellen() {
        double d1 = Math.pow(nf.getP(), 2);
        double d2 = (4 * nf.getA() * nf.getQ());
        double d = d1 - d2;
        if(d > 0) {
            double w = Math.sqrt(d);
            double vw = -nf.getP();
            double oB1 = vw + w;
            double oB2 = vw - w;
            double uB = 2 * nf.getA();

            double x1 = oB1 / uB;
            double x2 = oB2 / uB;

            Point2D[] returnArray = new Point2D[2];
            returnArray[0] = new Point2D(x1, 0);
            returnArray[1] = new Point2D(x2, 0);
            return returnArray;
        } else if(d == 0) {
            double oB = -nf.getP();
            double uB = 2 * nf.getA();

            Point2D[] returnArray = new Point2D[1];
            returnArray[0] = new Point2D(oB / uB, 0);
            return returnArray;
        } else {
            return null;
        }
    }
    
    public SPF getSPF() {
        return spf;
    }
    
    public NormalF getNF() {
        return nf;
    }
}


