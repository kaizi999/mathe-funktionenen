/*
Copyright Kai-Uwe Zimdars 2015
 */
package mathe;

/**
 *
 * @author Kai-Uwe
 */
public class SPF {
    
    double a;
    double verschiebungY;
    double verschiebungX;
    
    public SPF(double a,double verschiebungX,double verschiebungY) {
        this.a = a;
        this.verschiebungX = verschiebungX;
        this.verschiebungY = verschiebungY;
    }
    
    public double getA() {
        return a;
    }
    
    public double getVerschiebungX() {
        return verschiebungX;
    }
    
    public double getVerschiebungY() {
        return verschiebungY;
    }

    @Override
    public String toString() {
        if(verschiebungY < 0) {
            if (verschiebungX >= 0) {
                return "SPF: f(x) = " + a + " * (x^2 + " + Math.abs(verschiebungX) + ") - " + Math.abs(verschiebungY);
            } else {
                return "SPF: f(x) = " + a + " * (x^2 - " + Math.abs(verschiebungX) + ") - " + Math.abs(verschiebungY);
            }
        } else {
            if(verschiebungX >= 0) {
                return "SPF: f(x) = " + a + " * (x^2 + " + Math.abs(verschiebungX) + ") + " + Math.abs(verschiebungY);
            } else {
                return "SPF: f(x) = " + a + " * (x^2 - " + Math.abs(verschiebungX) + ") + " + Math.abs(verschiebungY);
            }
        }
    }

    public static SPF getSPF(NormalF nf) {
        double a = nf.getA();
        double verschiebungX = nf.getP() / 2;
        double verschiebungY = Math.pow(nf.getP() / 2, 2) * (-1) + nf.getQ();
        return new SPF(a, verschiebungX, verschiebungY);
    }
}
