package mathe;

import junit.framework.TestCase;
import org.junit.Test;

public class FunktionenTest extends TestCase {

    @Test
    public void testGetScheitel() throws Exception {
        Funktionen f = new Funktionen(new SPF(1, 3, 5));
        Point2D scheitel = new Point2D(-3, 5);
        assertEquals(scheitel.getX(), f.getScheitel().getX(), 0);
        assertEquals(scheitel.getY(), f.getScheitel().getY(), 0);
    }

    @Test
    public void testBerechneY() throws Exception {
        Funktionen f = new Funktionen(new NormalF(1, 0, 0));
        assertEquals(4, f.berechneY(2), 0);
    }

    @Test
    public void testNullstellen() throws Exception {
        Funktionen f = new Funktionen(new SPF(1, 0, -2));
        Point2D[] nullstellen = new Point2D[2];
        nullstellen[0] = new Point2D(1.4142, 0);
        nullstellen[1] = new Point2D(-1.4142, 0);
        assertEquals(nullstellen[0].getX(), f.nullstellen()[0].getX(), 0);
        assertEquals(nullstellen[1].getX(), f.nullstellen()[1].getX(), 0);
    }

    @Test
    public void testAbcNullstellen() throws Exception {
        Funktionen f = new Funktionen(new SPF(1, 0, -2));
        Point2D[] nullstellen = new Point2D[2];
        nullstellen[0] = new Point2D(1.4142, 0);
        nullstellen[1] = new Point2D(-1.4142, 0);
        assertEquals(nullstellen[0].getX(), f.abcNullstellen()[0].getX(), 0);
        assertEquals(nullstellen[1].getX(), f.abcNullstellen()[1].getX(), 0);
    }
}